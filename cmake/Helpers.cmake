cmake_minimum_required(VERSION 3.15)

macro(push VARNAME VALUE)
    list(APPEND PUSH_${VARNAME}_STACK  "${${VARNAME}}" )
    set(${VARNAME} ${VALUE})
endmacro()

macro(pop VARNAME)
    list(POP_FRONT PUSH_${VARNAME}_STACK ${VARNAME} )
endmacro()


macro(print_all_variables filter)
    message(STATUS "print_all_variables------------------------------------------{")
    get_cmake_property(_variableNames VARIABLES)
    foreach (_variableName ${_variableNames})
        string(REGEX MATCH "${filter}" REGEX_MATCHED "${_variableName}")
        if (REGEX_MATCHED)
            message("    ${_variableName}=${${_variableName}}")
        endif()
    endforeach()
    message(STATUS "print_all_variables------------------------------------------}")
endmacro()