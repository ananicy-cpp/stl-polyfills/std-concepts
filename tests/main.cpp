#include <concepts>

template <std::integral T>
T sum(T a, T b) { return a + b; }

int main() {
  return sum (3, 2);
}
