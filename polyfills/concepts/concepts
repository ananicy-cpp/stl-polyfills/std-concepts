#pragma once

#include <type_traits>

#pragma GCC system_header

#ifndef __cpp_lib_concepts
#define __cpp_lib_concepts 202002L
#endif

/**
 * "Inspired" from cppreference.com
 */

namespace std {
  namespace detail {
    template< class T, class U >
    concept SameHelper = std::is_same_v<T, U>;
  }

  template< class T, class U >
  concept same_as = detail::SameHelper<T, U> && detail::SameHelper<U, T>;

  template< class Derived, class Base >
  concept derived_from =
      std::is_base_of_v<Base, Derived> &&
      std::is_convertible_v<const volatile Derived*, const volatile Base*>;

  template < class T >
  concept integral = std::is_integral_v<T>;

  template < class T >
  concept signed_integral = std::integral<T> && std::is_signed_v<T>;

  template < class T >
  concept unsigned_integral = std::integral<T> && !std::signed_integral<T>;

  template < class T >
  concept floating_point = std::is_floating_point_v<T>;
}
